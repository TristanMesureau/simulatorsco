import numpy as np
from debug import *
from easyMesh import *
from maths3D import *
import trimesh
import pdb

modeRotation = 'xyz' # xyz for extrinsic rotations order, XYZ for intrinsic
accuracyFactor = 1
# A = 22.5
# WARNING ! following angles represents deformation projected angles on axes, but can't be used for the real deformity setting, but still accurate for having insights
# The real deformity would be obtained by the rotation A around the normal of ZY plane in Sangeorzan space, with the angle of Z angles difference between Cs1 and Cs2
angleDeformationFrontal = 45 #13 # plan frontal = rotation sur l'axe z dans 3dverse
angleDeformationSagittal = 0 #-19 # plan sagittal = rotation sur l'axe x dans 3dverse
angleTorsion = 45 #10 # plan transverse = rotation sur l'axe y dans 3dverse

# Press A to display the global transform (= 3dverse space)
# Press W and C to toggle display for wireframe and normals

# CREATING VECTORS IN 3DVERSE SPACE
yTdvCs1 = np.array([0,1,0])
# yTdvCs2 = np.array([0.707,0.707,0])
yTdvCs1Tri = alignCylinderOnVector(yTdvCs1, [0,0,0])
# yTdvCs2Tri = alignCylinderOnVector(yTdvCs2, [0,0,0])
yTdvCs2Tri = rotateToXYZ(yTdvCs1Tri, angleDeformationSagittal,angleTorsion,angleDeformationFrontal, modeRotation)
# yTdvCs2Tri = rotateToXYZ(yTdvCs2Tri, 0,angleTorsion,0, modeRotation) # /!\ chaining rotateToXYZ does not work so another line is needed
yTdvCs2AxisTri = trimesh.Trimesh(vertices=-yTdvCs2Tri.vertices,faces=yTdvCs2Tri.faces)
yTdvCs1Tri.visual.vertex_colors = (0,0,1.0)
yTdvCs2Tri.visual.vertex_colors = (0,0,1.0)
yTdvCs2AxisTri.visual.vertex_colors = (0,0,1.0)
xTdvCs1 = np.array([1,0,0])
# xTdvCs2 = np.array([0.707,0,0.707])
xTdvCs1Tri = alignCylinderOnVector(xTdvCs1, [0,0,0])
# yTdvCs2Tri = alignCylinderOnVector(xTdvCs2, [0,0,0]) # repère non orthonormé ! => rotateXYZ for both rotations
xTdvCs2Tri = rotateToXYZ(xTdvCs1Tri, angleDeformationSagittal,angleTorsion,angleDeformationFrontal, modeRotation)
# xTdvCs2Tri = rotateToXYZ(xTdvCs2Tri, 0,angleTorsion,0, modeRotation)
xTdvCs1Tri.visual.vertex_colors = (0,1.0,0)
xTdvCs2Tri.visual.vertex_colors = (0,1.0,0)
(yTdvCs1Tri+yTdvCs2Tri+xTdvCs1Tri+xTdvCs2Tri).show()

# # CONVERT INTO SANGEORZAN SPACE
zCs1 = np.array([yTdvCs1[2], yTdvCs1[0], yTdvCs1[1]])
# zCs2 = np.array([yTdvCs2[2], yTdvCs2[0], yTdvCs2[1]])
zCs1Tri0 = alignCylinderOnVector(zCs1, [0,0,0])
# zCs2Tri = alignCylinderOnVector(zCs2, [0,0,0])
modeRotation = 'yzx'  # xyz = yzx dans sangeorzan ; here we permute rotations of 3dverse space
xyzCs2Matrix = getRotationMatrixFromXYZ(angleDeformationSagittal,angleTorsion,angleDeformationFrontal, modeRotation)
# yCs2Matrix = getRotationMatrixFromXYZ(0,0,angleTorsion, modeRotation)
zCs2Tri0 = rotateToXYZ(zCs1Tri0, angleDeformationSagittal,angleTorsion,angleDeformationFrontal, modeRotation) # xyz = yzx dans sangeorzan
# zCs2Tri = rotateToXYZ(zCs2Tri, 0,0,angleTorsion, modeRotation) # /!\ chaining rotateToXYZ does not work so another line is needed
zCs2AxisTri0 = trimesh.Trimesh(vertices=-zCs2Tri0.vertices,faces=zCs2Tri0.faces)
yCs1 = np.array([xTdvCs1[2], xTdvCs1[0], xTdvCs1[1]])
yCs1Tri = alignCylinderOnVector(yCs1, [0,0,0])
yCs2Tri = rotateToXYZ(yCs1Tri, angleDeformationSagittal,angleTorsion,angleDeformationFrontal, modeRotation)
# yCs2Tri = rotateToXYZ(yCs2Tri, 0,0,angleTorsion, modeRotation)
zCs1Tri0.visual.vertex_colors = (0,0,1.0)
zCs2Tri0.visual.vertex_colors = (0,0,1.0)

# Test de la matrice de permutation au lieu de changer les points
modeRotation = 'xyz'
xyzTdvCs2Matrix = getRotationMatrixFromXYZ(angleDeformationSagittal,angleTorsion,angleDeformationFrontal, modeRotation)
zCs1Tri = yTdvCs1Tri
zCs2Tri = yTdvCs2Tri
yCs1Tri = xTdvCs1Tri
yCs2Tri = xTdvCs2Tri
zCs2AxisTri = trimesh.Trimesh(vertices=-zCs2Tri.vertices,faces=zCs2Tri.faces)

zCs1Tri.visual.vertex_colors = (0,0,1.0)
zCs2Tri.visual.vertex_colors = (0,0,1.0)
yCs1Tri.visual.vertex_colors = (0,1.0,0)
yCs2Tri.visual.vertex_colors = (0,1.0,0)
zCs2AxisTri.visual.vertex_colors = (0,0,1.0)

# CALCULATE ROTATION MATRIX
# anglesXYZ = computeEulerAnglesForVectorToVectorAlignment(zCs1, zCs2, modeRotation, accuracyFactor)
# print("anglesXYZ = ", anglesXYZ)
# Mc = getRotationMatrixFromXYZ(anglesXYZ[0], anglesXYZ[1], anglesXYZ[2], modeRotation)
# Mc = computeTotalRotationMatrix(zCs2Matrix,yCs2Matrix)
Mc = xyzTdvCs2Matrix # xyzCs2Matrix
anglesXYZ = convertRotationMatrixToEulerAngles(Mc, modeRotation)
print("XYZ angles from Mc = ", anglesXYZ)
print("Mc = ", Mc)
# Mr[0] = Mc*MatriceDePermutation https://fr.wikipedia.org/wiki/Matrice_de_permutation
# Mper = np.array([[0,0,1],[1,0,0],[0,1,0]]) # permutations needed for [2,0,1]
# Mper = np.array([[0,1,0],   # Mper with inverted columns and lines
#                 [0,0,1],
#                 [1,0,0]])
# Mr = np.array(Mc)[:3,:3] * Mper # test sans matrice de permutation, ce sont les points qui ont été recalés qui subissent la permutation
# Mc = np.array(Mc)[:3,:3]
# Mr = np.identity(3)
# Mr[:,0],Mr[:,1],Mr[:,2] = Mc[:,2],Mc[:,0],Mc[:,1]
# matrice de permutation comme rotation : -90 -90 0 pour xyz extrinsèque ou bien -90 0 -90 en intrinsèque (XYZ) dans le repère 3dverse
# cs2Tri = TransformGizmo()
# cs2Tri.applyRotation(-90,-90,0,'xyz') # fonctionne !
# cs2Tri.show()
# Mper = getRotationMatrixFromXYZ(-90,-90,0,'xyz')
# cs2Tri = TransformGizmo()
# cs2Tri.applyRotation(-90,0,-90,'XYZ') # fonctionne aussi !
# cs2Tri.show()
# Get the rotation matrix in the Sangeorzan space
anglesXYZ = convertRotationMatrixToEulerAngles(Mc, modeRotation)
print("Angles déformation = ", anglesXYZ)
vectorReference = [0,1,0]
vectorRotated = transform_points([vectorReference], Mc, translate=False)[0]
# vectorRotated = transformPointWithMat4(vectorReference, Mc, translate=False) # wrong direction for rotation !
anglesXYZ = computeEulerAnglesForVectorToVectorAlignment(vectorReference, vectorRotated, modeRotation, accuracyFactor)
print("Angles à corriger dans le repere 3dverse = ", anglesXYZ) # doit être pareil que la déformation !
vectorReferenceTri = alignCylinderOnVector(vectorReference, [0,0,0])
vectorRotatedTri = alignCylinderOnVector(vectorRotated, [0,0,0])
vectorReferenceTri.visual.vertex_colors = (1.0,0,0)
vectorRotatedTri.visual.vertex_colors = (1.0,0,0)
(zCs1Tri+zCs2Tri+vectorReferenceTri+vectorRotatedTri).show()
# now in sangeorzan space :
vectorReference = vectorReference[2], vectorReference[0], vectorReference[1]
vectorRotated = vectorRotated[2], vectorRotated[0], vectorRotated[1]
print("Vector rotated = ", vectorRotated)
vectorReferenceTri = alignCylinderOnVector(vectorReference, [0,0,0])
vectorRotatedTri = alignCylinderOnVector(vectorRotated, [0,0,0])
vectorReferenceTri.visual.vertex_colors = (1.0,0,0)
vectorRotatedTri.visual.vertex_colors = (1.0,0,0)
(zCs1Tri0+zCs2Tri0+vectorReferenceTri+vectorRotatedTri).show()
# pdb.set_trace()
anglesXYZ = computeEulerAnglesForVectorToVectorAlignment(vectorReference, vectorRotated, modeRotation, accuracyFactor)
print("Angles à corriger dans le repere Sangeorzan = ", anglesXYZ)
Mr = getRotationMatrixFromXYZ(anglesXYZ[0], anglesXYZ[1], anglesXYZ[2])
# Mr = alignMatrix(vectorReference, vectorRotated)
# MperExtrinsic = getRotationMatrixFromXYZ(-90,-90,0, 'xyz')
# Mper01 = getRotationMatrixFromXYZ(-90,0,0, 'XYZ')
# Mper02 = getRotationMatrixFromXYZ(0,0,-90, 'XYZ')
# Mper = computeTotalRotationMatrix(Mper01, Mper02)
# pdb.set_trace()
# Mr = computeTotalRotationMatrix(Mc, Mper)
print("Mr = ", Mr) # TODO : chercher pourquoi Mr n'a pas la valeur attendue, sachant que le vecteur rotated dans sangeorzan par cette matrice est correct (computeEulerAnglesForVectorToVectorAlignment ou getRotationMatrixFromXYZ est peut-être le problème)
xyzSangeorzanCs2Matrix = getRotationMatrixFromXYZ(angleDeformationSagittal,angleTorsion,angleDeformationFrontal, modeRotation)
print("Expected Mr = ", xyzSangeorzanCs2Matrix)
(zCs1Tri+zCs2Tri+yCs1Tri+yCs2Tri).show()

# CALCULATE PLANE NORMAL
# Mc = matrice de correction [4;4] dans le repère 3Dverse
# Mr = matrice de rotation [3;3] dans le repère Sangeorzan
def limitResult(result , limit):
    if(-limit <= result <= limit): return result
    resultLimited = None
    if result > limit: resultLimited = limit
    if result <-limit: resultLimited = -limit
    print("WARNING ! result = ", result, " is now limited to ", resultLimited) 
    return resultLimited
beta = math.acos(limitResult(0.5*(Mr[0,0]+Mr[1,1]+Mr[2,2]-1), 1))
k_matrix = np.array([[Mr[1,2]-Mr[2,1]],[Mr[2,0]-Mr[0,2]],[Mr[0,1]-Mr[1,0]]])
k = (1/(2*math.sin(beta))) * k_matrix

def normalize(vec3):
    norm = math.sqrt(vec3[0]*vec3[0]+vec3[1]*vec3[1]+vec3[2]*vec3[2])
    return np.array(vec3)/norm
k = [k[0][0],k[1][0],k[2][0]]
k = normalize(k)
# Reconvertir k dans l'espace 3dverse :
(k[0], k[1], k[2]) = (k[1], k[2], k[0])
print("k = ", k)
print("beta radians = " + str(beta))
beta = beta*180/math.pi
print("beta degrés = " + str(beta))

# Debug azimuth (phi) and elevation (theta)
theta = math.atan2(math.sqrt(k[0]**2+k[1]**2), k[2])
phi = math.atan2(k[1], k[0])
print("Azimuth en degrés (phi) = " + str(phi*180/math.pi))
print("Elevation en degrés (theta) = " + str(theta*180/math.pi))

# CREATE PLANE MESH
# planeOrigin = np.array(ezEntity.getProp(GLOBAL_POSITION))
planeNormal = [0,1,0]

# Apply the orientation of the plane entity :
vertices = [ [-1,0,1], [1,0,1] , [1,0,-1], [-1,0,-1], planeNormal, [0,0,0]]
faces =  [ [0,1,2], [0,2,3], [4,5,4] ]
planeMesh = trimesh.Trimesh(vertices=vertices, faces=faces, process=False)
anglesXYZ = computeEulerAnglesForVectorToVectorAlignment(planeNormal, k, modeRotation, accuracyFactor)
planeRotationMatrix = getRotationMatrixFromXYZ(anglesXYZ[0], anglesXYZ[1], anglesXYZ[2], modeRotation)
planeMesh.apply_transform(planeRotationMatrix)
planeMesh.visual.vertex_colors = (0,0,0)
# planeMesh = updateMesh(None, meshEntity=ezEntity.tdvEntity, meshTri=planeMesh, enableTrimeshProcess=False)
# planeNormal = np.array(planeMesh.vertices[4]) - np.array(planeMesh.vertices[5])
# planeNormal = planeNormal/np.linalg.norm(planeNormal)

# (planeMesh+zCs1Tri+zCs2AxisTri).show()

# SIMULATE FIXING
if(k[1]<0): beta *= -1
correctedVertices = rotateAroundVector(zCs2AxisTri.vertices, np.array(k), beta, np.array([0,0,0]))
zCs2AxisCorrectedTri = trimesh.Trimesh(vertices=correctedVertices, faces=zCs2AxisTri.faces)
zCs2AxisCorrectedTri.visual.vertex_colors = (0,1.0,1.0)
(planeMesh+zCs1Tri+zCs2AxisTri+zCs2AxisCorrectedTri).show()
# np. set_printoptions(suppress=True)
# pdb.set_trace()
